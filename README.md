# Zaney's Website

My name is Tyler 👋, but I go by Zaney online. I have been using Linux for years now and really enjoy what the ecosystem has to offer. I create videos on my experience with Linux, share my configuration files, and try to share what I've learned with anyone in the community.

If you are interested in what I have to say or how I built and manage my website, then this is definitely for you. You'll find my website to be an ad free zone, which hopefully is a nice break from the rest of the web. If you want to help keep it this way please consider supporting me.

<a href="https://www.buymeacoffee.com/notzaney" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 50px !important;width: 200px !important;" ></a>
