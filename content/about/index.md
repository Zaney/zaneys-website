---
title: "About"
date: 2023-11-27
draft: false

showDate : false
showDateUpdated : false
showHeadingAnchors : false
showPagination : false
showReadingTime : false
showTableOfContents : true
showTaxonomies : false 
showWordCount : false
showSummary : false
sharingLinks : false
showEdit: false
showViews: true
showLikes: false
showAuthor: true
layoutBackgroundHeaderSpace: false
---

My name is Tyler 👋, but I go by Zaney online. I have been using Linux for years now and really enjoy what the ecosystem has to offer. I create videos on my experience with Linux, share my configuration files, and try to share what I've learned with anyone in the community.

## 🧛 What Am I About?

I am an avid [Hyprland](https://hyprland.org/) user and have for a long time enjoyed using tiling window managers. I enjoy tinkering with things and having a really polished experience. When I first really got interested in learning Linux, it was because of people who had systems that looked like mine. So hopefully I can peek your interest in Linux & make the process of getting to where I am much, much simpler!

I have a passion for software and hardware. I have spent a lot of my time coding, 3D modeling, and making videos. I enjoy helping other people and love customizing my system.

{{< gitlab projectID="53038185" >}}

Above is my NixOS configuration, it's configured as a [NixOS Flake](https://nixos.wiki/wiki/Flakes) for some added reliability. I have worked very hard to make my system not only look very good, but also be as easy as possible to setup yourself. The entire idea is you have a simple script for installation and then you have a folder to make changes, tinker, and learn to your hearts content! I hope this repository serves as a useful resource in your NixOS or Linux journey.

I used to call Arch Linux home and loved using it for years. I would frequently be trying something new out, but would always seem to come back to it as my distribution of choice. That has since changed and I have become engrossed in the Nix ecosystem. Some of the things it makes so incredibly simple and yet you can do some really cool complex things with it at the same time!

I provide paid NixOS & general Linux support over on [Patreon](https://www.patreon.com/akazaney) if you are interested in having one on one help with something. I also do contract work for projects including websites, web apps, and software development. If you are interested in having me work with you and/or your team please reach out over [Email](mailto:tylerzanekelley@gmail.com) or [Discord](https://discord.gg/2cRdBs8).

## 🎓 Education

{{< timeline >}}
{{< timelineItem icon="lightbulb" header="Penn Foster" badge="2012-2016" subheader="High School Diploma" >}}
I originally went to Ravenwood High School. However, I found out that I was missing a half credit for physical education and would have to come back for half a semester. This meant taking the class with freshman as well. So instead found out about Penn Foster and it allowed me to transfer and make up the credits I needed. Was a great online experience.
{{< /timelineItem >}}
{{< timelineItem icon="youtube" header="Tutorials & Guides" badge="?-current" subheader="To this day I learn constantly in my free time without it costing me money." >}}
I have watched hours upon hours of videos teaching me everything from, how to use my text editor and terminal more effeciantly too HLSL shader coding for use in Unity. 
I've always loved learning especially within the context of solving a problem I face.
{{< /timelineItem >}}
{{< /timeline >}}

---

I am almost totally self taught and have used very little paid resources to get where I am knowledge wise.

---

<table>
    <thead>
        <tr>
            <th>School</th>
            <th>Link</th>
            <th>Degree</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><img class="customEntitityLogo" src="pennfosterlogo.png"/></td>
            <td><a href="https://www.pennfoster.edu/" target="_blank">Penn Foster</a></td>
            <td>High School Diploma</td>
            <td>2016</td>
        </tr>
        <tr>
            <td rowspan=5><img class="customEntitityLogo" src="freecodecamp.png"/></td>
            <td rowspan=5><a href="https://freecodecamp.org/" target="_blank">freeCodeCamp</a></td>
        </tr>
         <tr>
            <td>Frontend Web Development Bootcamp Course</td>
            <td>2023</td>
        </tr>
        <tr>
            <td>JavaScript - Full Course</td>
            <td>2022</td>
        </tr>
        <tr>
            <td>Python - Full Course</td>
            <td>2022</td>
        </tr>
        <tr>
            <td>SQL - Full Course</td>
            <td>2022</td>
        </tr>
        <tr>
            <td rowspan=3><img class="customEntitityLogo" src="udemy.png"/></td>
            <td rowspan=3><a href="https://www.udemy.com/" target="_blank">Udemy</a></td>
        </tr>
        <tr>
            <td>Rigging & Animating FPS Arms in Blender</td>
            <td>2021</td>
        </tr>
        <tr>
            <td>Unity Multiplayer: C# & Networking</td>
            <td>2021</td>
        </tr>
    </tbody>
</table>

## 🧙 Expertise

### Web & Software Development:

- **Languages:** Python, C#, GDScript, Lua, Javascript, Typescript.
- **Frameworks:** Django, THREEjs, Hugo, React.
- **Frontend:** HTML, CSS.
- **Text Editing:** Neovim.

### System Administration:

- **Operating Systems:** NixOS, Linux, OpenBSD.
- **Scripting:** Bash, Systemd.
- **Configuration Management:** Nix.

### Community Engagement:

- **Brand Development:** Experience in building and shaping online identities.
- **Community Management:** Proven track record in fostering and nurturing online communities.
- **Customer Service:** Dedicated to providing excellent customer support.

### YouTube Content:

Check out my [YouTube channel](https://www.youtube.com/c/ZaneyOG) for in-depth tutorials and insights into NixOS, Linux, and other related technologies. I believe in making complex subjects accessible to everyone, fostering a community of learners.

## 💼 Current Projects

- **Website Development:** Currently hosting & building websites for clients as well as making this website comprehensive to centralize resources and further enhance the learning experience for my audience.
- **ZaneyOS:** My systems configuration is a constantly improving and evolving project not only aimed at being my perfect system, but also a learning vehicle for all those interested.

## 🦊 Gitlab Repositories

Below you will find some of my current and past work on different things:

<table>
    <thead>
        <tr>
            <th>Logo</th>
            <th>Title</th>
            <th>Description</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
         <tr>
            <td><img class="customEntitityAlbum" style="background-color:transparent" src="zaneyos.avif"/></td>
            <td>ZaneyOS</td>
            <td>An incredible NixOS configuration with a simple install script and easy to understand options file. Complete with simple theme changing, a keybinding dropdown in the bar, and beautiful wallpaper switcher.</td>
            <td><a target="_blank" href="https://gitlab.com/Zaney/zaneyos">repo</a></td>
        </tr>
         <tr>
            <td><img class="customEntitityAlbum" style="background-color:transparent" src="myeyes.avif"/></td>
            <td>Dotfiles</td>
            <td>A beautiful, fast, and modular config for Hyprland designed for hidpi screens.</td>
            <td><a target="_blank" href="https://gitlab.com/Zaney/dotfiles">repo</a></td>
        </tr>
        <tr>
            <td><img class="customEntitityAlbum" style="background-color:transparent" src="joshprofilepic.avif"/></td>
            <td>Rogers Mortgage Group</td>
            <td>A beautifully simple mortgage website made with pure HTML & CSS.</td>
            <td><a target="_blank" href="https://rogersmortgagegroup.com">site</a></br><a target="_blank" href="https://gitlab.com/Zaney/rogersmortgage">repo</a></td>
        </tr>
         <tr>
            <td><img class="customEntitityAlbum" style="background-color:transparent" src="openpicom.avif"/></td>
            <td>Open-Picom</td>
            <td>A fork of Pijulius's Picom, with great animations, that has been modified to build on OpenBSD.</td>
            <td><a target="_blank" href="https://gitlab.com/Zaney/picom">repo</a></td>
        </tr>
    </tbody>
</table>

---
