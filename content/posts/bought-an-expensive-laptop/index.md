---
title: "Why I Bought An Extremely Expensive Laptop"
description: "I ended up deciding that a M3 Pro MacBook Pro 14 inch, was going to be my reckless purchase of the year. This is just me outlining why I bought it, my plans for it, and the goal. As I do see this laptop as an investment."
summary: "I ended up deciding that a M3 Pro MacBook Pro 14 inch, was going to be my reckless purchase of the year. This is just me outlining why I bought it, my plans for it, and the goal. As I do see this laptop as an investment."
categories: ["Opinion","Apple"]
tags: ["Laptop","MacBook"]
#externalUrl: ""
date: 2024-04-15
draft: false
authors:
  - Zaney
---

This is definitely more like a journal entry than any of my other posts... 

So, I ended up deciding that a M3 Pro MacBook Pro 14 inch was going to be my reckless purchase of the year. This is just me outlining why I bought it, my plans for it, and the goal. As I do see this laptop as an investment.

## 🍎 Why Buy Apple?

This is obviously the real question that many will have when it comes to specifically what I have bought. I went with Apple mainly due to the incredible experience I had using my sister's M1 MacBook Air last year. Even that base model surprised the crap out of me with what it could do.

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/l0HlwfPZ69Y2XFLHO" width="100%" height="100%" frameBorder="0" style="position:absolute" class="giphy-embed" allowFullScreen></iframe></div>

### 🔋 Battery Life & Power Efficiency

I am going to be traveling more and really want something to last while I edit videos, write blog posts like this, and manage all of my socials. The performance on these MacBooks while maintaining incredible battery life is a really hard thing to pass up on.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### 📚 Productivity Improvements

I am really bad about staying on top of things and managing a schedule consistently. I find on MacOS this is not nearly as much of a problem. Not to mention that the fact I have an iPhone means all my notifications are going to be tightly integrated. This definitely helps me not miss things I otherwise would.

### 🏎️ Performance & Noise

When it comes to the creative tasks the new Apple silicon is really impressive! This thing destroys most anything I throw at it all the while staying pretty much silent. Not to mention I can do all of this on battery for so much longer than almost any other laptop out there.

You can give this video a look to see a great video on the advantages of a MacBook from another Linux users perspective:

{{< youtube X0DIHlnD_S0 >}}

## 🚔 Driving Force

For about 6 months I have been meaning to take my home network seriously and get a proper home-lab started. This switch has forced me into taking my main machine and making it the family server. This machine is running a 8 core / 16 thread CPU, 32GB RAM, 16+ TB of storage, running Proxmox, and an AMD GPU for a gaming virtual machine.

### 💵 I Want To Invest In Creating Better Videos

You may have noticed that over the past year I have been upgrading my gear. I have gotten a better audio interface, DSLR camera, improving lighting. The next move for me once I have my lighting and audio fixed is to start improving the editing quality. 

#### 🧙 DaVinci Resolve

As much as I may love Kdenlive for what it is. It is also objectively not the best video editor out there on the market. I really love DaVinci Resolve as it has just so much you can do with it. Not to mention it is free as in price and performs incredibly on the new Apple Silicon. 

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

Look, you can edit videos and do good work on Linux and with editors like Kdenlive. It's just there's some really cool things I can do with DaVinci. I am also going to be subscribing to a service like Motion Array and there I will have a ton of assets to improve the quality of my videos. These services also provide me with tons of templates and effects designed to be used with DaVinci Resolve!

### 🧑‍🔧 Time For A Proper Home-Lab

I have always wanted a singular powerhouse server that all of my family can use for all of our needs. This purchase has forced me, or given me, the opportunity to take what was my main computer and convert it into our family server.

It's running Proxmox with a Jellyfin server, NFS Shared Drive, Nextcloud, ZaneyOS Development VM, & available to the outside world using Twingate. We are slowly building our media library at home so we can leave the over-priced streaming services and be able to view our media here at home regardless of if the internet is up.

## 👋 Conclusion

I am having a blast right now using this MacBook, not a fan of file management here at all - probably never will be. This laptop is an incredible device and it freakin' better be with how much I paid! 🤣

I am very busy with getting my home network figured out and so much more in personal life. Very excited to get back to making regular video content and hope you guys enjoy. *Got to go, but I will definitely see you around!*