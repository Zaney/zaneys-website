---
title: "Choosing a Linux Distribution. Should It Be NixOS & How To Install NixOS?"
description: "Choosing a Linux distribution is a crucial decision for anyone venturing into the world of open-source operating systems. Among the many options available, NixOS stands out as a unique and innovative choice. Unlike traditional Linux distributions, NixOS offers a fundamentally different approach to package management and system configuration, making it an excellent option for both developers and system administrators."
summary: "Choosing a Linux distribution is a crucial decision for anyone venturing into the world of open-source operating systems. Among the many options available, NixOS stands out as a unique and innovative choice. Unlike traditional Linux distributions, NixOS offers a fundamentally different approach to package management and system configuration, making it an excellent option for both developers and system administrators."
categories: ["Software"]
tags: ["NixOS","Choosing a Linux Distro","Linux"]
#externalUrl: ""
date: 2024-12-16
draft: false
authors:
  - Zaney
---

Choosing a <a target="_blank" href="https://amzn.to/3ZIxUgN">Linux distribution</a> is a crucial decision for anyone venturing into the world of open-source operating systems. Among the many options available, <a target="_blank" href="https://amzn.to/3ZXXf7y">NixOS</a> stands out as a unique and innovative choice. Unlike traditional Linux distributions, NixOS offers a fundamentally different approach to package management and system configuration, making it an excellent option for both developers and system administrators. By understanding what sets <a target="_blank" href="https://nixos.org/">NixOS</a> apart, you can better appreciate its strengths and decide if it’s the right fit for your needs.

## What Is NixOS?

<a target="_blank" href="https://nixos.org/">NixOS</a> is a Linux distribution built around the Nix package manager, a powerful tool that introduces a declarative and functional approach to package management. The hallmark feature of NixOS is its declarative configuration model, which allows users to define their entire system—from installed packages to system services and configurations—in a single configuration file. This model ensures that every aspect of the system is reproducible, making it incredibly easy to replicate the same setup on multiple machines or restore a specific configuration after an upgrade or hardware change. For developers who frequently work across different environments, this capability can save hours of manual setup and troubleshooting.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### The Package Manager

One of the standout features of <a target="_blank" href="https://nixos.org/">NixOS</a> is its package repository, which is among the largest in the Linux ecosystem. With over 80,000 packages available, <a target="_blank" href="https://nixos.org/">NixOS</a> surpasses the repositories of most mainstream Linux distributions, such as Ubuntu, Fedora, and Arch Linux. This vast selection means that users rarely have to worry about the availability of software, whether it’s a common application like Firefox or a niche library for development. Furthermore, the Nix package manager ensures that each package is built in isolation, eliminating issues caused by conflicting dependencies—a common headache in other distributions. This guarantees a level of stability and predictability that is difficult to achieve elsewhere.

### Rollback / Generations System

Another compelling reason to choose NixOS is its immutability and rollback capabilities. Because NixOS treats the entire system configuration as a single, declarative entity, any changes are applied atomically. This means that you can safely experiment with new configurations or software updates without the fear of breaking your system. If something goes wrong, NixOS allows you to roll back to a previous working state effortlessly. This feature is particularly useful for system administrators managing critical infrastructure, where downtime and configuration errors can have significant consequences.

## What Makes NixOS Great?

NixOS also excels in automation and scalability, making it a popular choice for organizations running large-scale deployments. By leveraging the declarative configuration model, system administrators can easily define and manage consistent environments across hundreds or thousands of machines. This is invaluable in modern DevOps workflows, where reproducibility and consistency are key. Additionally, NixOS integrates seamlessly with containerization technologies like Docker and Kubernetes, further enhancing its utility in cloud-native environments.

For developers, NixOS offers a unique advantage in the form of reproducible development environments. By using Nix to define all the dependencies and tools required for a project, developers can ensure that their code runs identically on any machine. This eliminates the “works on my machine” problem and fosters better collaboration across teams. Whether you’re working on a personal project or contributing to an open-source initiative, NixOS provides the tools to streamline your workflow and reduce the friction associated with setting up and maintaining development environments.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

## Heard NixOS Can Be Complicated?

The learning curve associated with <a target="_blank" href="https://nixos.org/">NixOS</a> can be steep, particularly for users who are accustomed to traditional Linux distributions. The declarative approach requires a shift in mindset, as does working with the Nix expression language used to define configurations. However, the benefits of mastering NixOS far outweigh the initial challenges. The distribution’s documentation is comprehensive, and an active community is always available to help newcomers. For those willing to invest the time to learn, NixOS offers unparalleled control, stability, and flexibility.

## A Complete Guide to Installing NixOS

### Step 1: Preparing for Installation

1. **Check System Requirements**

Before you begin, ensure your hardware meets the following requirements:

- **Processor:** x86_64 or ARM64 architecture.
- **RAM:** At least 2 GB for basic setups; 4 GB or more is recommended.
- **Storage:** Minimum of 10 GB; more is required for larger setups or additional packages.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

2. **Download the NixOS ISO**

Visit the NixOS download page.

Choose the appropriate ISO image for your system:

- **Graphical ISO:** Comes with a desktop environment for easier navigation.
- **Minimal ISO:** Recommended for advanced users or server setups.

Download the ISO and verify its checksum to ensure file integrity.

{{< alert >}}
**Warning!** You definitely want to download either the GNOME or KDE Graphical ISO's! This will have a simple graphical install that will make the process much more straightforward.
{{< /alert >}}

3. **Create a Bootable USB**

Use a tool like `dd` (Linux), Rufus (Windows), or Balena Etcher (cross-platform) to create a bootable <a target="_blank" href="https://amzn.to/3VLMbrF">USB</a>.

For example, on Linux:

```bash
sudo dd if=/path/to/nixos.iso of=/dev/sdX bs=4M status=progress
```

Replace `/path/to/nixos.iso` with the downloaded ISO path and `/dev/sdX` with your USB drive.

4. **Backup Your Data**

Installing NixOS involves formatting partitions, which will erase existing data. Backup important files before proceeding.

### Step 2: Booting into the NixOS Live Environment

1. Insert the bootable <a target="_blank" href="https://amzn.to/3VLMbrF">USB</a> and restart your system.
2. Access the BIOS/UEFI menu (commonly by pressing `F2`, `F12`, `Esc`, or `Del` during boot).
3. Set the USB drive as the primary boot device.
4. Save changes and boot into the NixOS live environment.

Once booted, you’ll see a terminal or graphical interface, depending on the ISO chosen.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="autorelaxed"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9202319200"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### Step 3: Partitioning the Disk

{{< alert >}}
**Warning!** This section and the next is only really relevant if you choose to do a minimal installation.
{{< /alert >}}

#### Identify Your Disk

Run the following command to list available disks:

```bash
lsblk
```

#### Partition the Disk

Replace `/dev/sdX` with your target disk. Here’s an example partitioning scheme:

1. **Create a GPT partition table:**

```bash
parted /dev/sdX -- mklabel gpt
```

2. **Create partitions:**

- EFI System Partition (ESP):

```bash
parted /dev/sdX -- mkpart ESP fat32 1MiB 512MiB
parted /dev/sdX -- set 1 boot on
```

- Root partition:

```bash
parted /dev/sdX -- mkpart primary ext4 512MiB 100%
```

3. **Format the partitions:**

```bash
mkfs.fat -F32 /dev/sdX1  # Format the EFI partition
mkfs.ext4 /dev/sdX2      # Format the root partition
```

4. **Mount the partitions:**

```bash
mount /dev/sdX2 /mnt
mkdir -p /mnt/boot
mount /dev/sdX1 /mnt/boot
```

### Step 4: Installing NixOS

#### Generate the Configuration Files

Run the following command to generate the default system configuration:

```bash
nixos-generate-config --root /mnt
```

This creates two files:

- `/mnt/etc/nixos/configuration.nix`: The main configuration file.
- `/mnt/etc/nixos/hardware-configuration.nix`: Auto-detected hardware settings.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="autorelaxed"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9202319200"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

#### Edit Configuration.nix

Open `/mnt/etc/nixos/configuration.nix` with your <a target="_blank" href="https://amzn.to/3BopZNw">preferred text editor</a> (e.g., `nano` or `vim`) to customize the system configuration. Here are some common additions:

- **Set the Timezone:**

```bash
time.timeZone = "America/New_York";
```

- **Enable a Desktop Environment (optional):** For GNOME:

```bash
services.xserver.enable = true;
services.xserver.displayManager.gdm.enable = true;
services.xserver.desktopManager.gnome.enable = true;
```

- **Add Users:**

```bash
users.users.yourusername = {
  isNormalUser = true;
  extraGroups = [ "wheel" "networkmanager" ];
};
```

- **Set a Bootloader:** For UEFI systems:

```bash
boot.loader.systemd-boot.enable = true;
boot.loader.efi.canTouchEfiVariables = true;
```

#### Start the Installation

Execute the following command to install NixOS:

```bash
nixos-install
```

During the installation, you’ll be prompted to set a root password. Once complete, reboot the system:

```bash
reboot
```

Remove the <a target="_blank" href="https://amzn.to/3VLMbrF">USB drive</a> to boot into your new NixOS installation.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### Step 5: Post-Installation Configuration

#### Update Your System

Keep your system up-to-date by running:

```bash
sudo nixos-rebuild switch --upgrade
```

#### Install Additional Packages

Add desired packages by editing `/etc/nixos/configuration.nix`. For example:

```bash
environment.systemPackages = with pkgs; [ vim firefox git ];
```

#### Apply changes with:

```bash
sudo nixos-rebuild switch
```

#### Enable Services

To enable a service, such as ssh, add the following to `configuration.nix`:

```bash
services.openssh.enable = true;
```

#### Rebuild the system:

```bash
sudo nixos-rebuild switch
```

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

## Conclusion

In conclusion, NixOS is not just another Linux distribution, it is a paradigm shift in how systems are managed and maintained. Its declarative configuration model, extensive package repository, and robust rollback capabilities make it an attractive choice for developers, system administrators, and anyone seeking a modern approach to Linux. While it may not be the most user-friendly distribution for beginners, the long-term benefits of reproducibility, stability, and scalability make it a standout option in the crowded Linux landscape. If you’re looking for a Linux distribution that prioritizes innovation and reliability, NixOS deserves serious consideration.