---
title: "How To Add Ads To Hugo Website Using Blowfish Theme"
description: "An easy to understand guide for monetizing your hugo website. This is made for those of you using the same hugo theme I use, Blowfish."
summary: "An easy to understand guide for monetizing your hugo website."
categories: ["How To","Tools"]
tags: ["Tutorial","Development","Hugo"]
#externalUrl: ""
date: 2024-12-12
draft: false
authors:
  - Zaney
---

You've completed your beautiful Hugo website and are ready to monetize your new creation. This simple guide for adding ads to your Hugo website is exactly what you need. This guide will be focused primarily on those using the Blowfish theme for Hugo. So if you want to make money using Google Adsense this is definitely a worth while read.

## What You Will Need

- Have a Google account.
- Website built using Hugo. You also need to be using the Blowfish theme for Hugo for this to be fully relevant.

## Required Steps:

### Create The Adsense Account

- Visit the <a target="_blank" href="https://www.google.com/adsense/start/">Google Adsense</a> site.
- Select Sign In button at the top.
- Login with your Google account.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### Add Your Website To Adsense

1. Click on Sites.

![Sites for Adsense](img/sites.avif)

2. Click on Add Site.

![Adding Site for Adsense](img/addsite.avif)

3. Enter the URL of your website in the text box and press save.

![Adding Site URL for Adsense](img/entersiteaddress.avif)

4. Copy the Adsense code snippet. We will use this to integrate Adsense into your website built with Hugo.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### Add The Adsense Code Snippet To Hugo Website

1. You will need to create the folder *layouts/partials* in the root directory of your website if you don't have it already. Create a file named *extend-head.html* this is where you will need to put the code snippet Adsense gave you. This file can contain anything you want added to the head of your website when using the Blowfish Hugo theme.  

![The extend-head.html file needed for Adsense on Blowfish Hugo Website](img/extendhead.avif)

{{< alert >}}
**Warning!** Blowfish is built to automatically use the `extend-head.html` file. Other themes for Hugo may not use a similar approach!
{{< /alert >}}

2. Deploy your website and click on request review in Adsense. If successful then after your review you will be able to serve ads on your website. However, there is more to do:

#### You Need One Extra File

You can go ahead and upload your *ads.txt* file, Google will prompt you to upload an *ads.txt* file into the root directory of your webpage.

1. You can add this by clicking on your website in the Sites page of Adsense and selecting the dropdown for *Verify site ownership*. There you will see the text to add to the file.

2. Create the folder `static` in the root folder of your website and create the `ads.txt` file in that folder. Then paste the text from Adsense and save.

3. Now just publish your changes and you are done.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

## While You Are Waiting For Approval

This is a great time to plan out where you want the ads to be on your website.

1. Click on the Ads option:

![The Ads option on the left side of Adsense](img/adsoption.avif)

2. Click on the Edit button (the pencil icon) on the bottom right side of the image below:

![The Edit option to the bottom right](img/editbutton.avif)

3. On the right side of the Adsense window, you will be able to make changes and see how the ads will be displayed on your website. Play with the options until you are happy with the result.

![The settings available for editing ads for your website](img/adsettings.avif)

*The ad options shown above are mainly for auto ads. You can also add other types of ads.*

4. Depending on whether you want to specify where ads will be inside of articles, you can choose to do this as well. To begin click on the Ads tab as shown in step 1 of this section.

5. Select the by ad unit tab:

![The section called by unit on Adsense](img/adunit.avif)

6. You can choose the ad unit that you want and select the ad that you wish to insert into your website.

7. Name the ad unit, save it, and get code.

![Create your own ad and get the code](img/testad.avif)

8. Copy the `HTML` from the ad that Adsense gave you and paste it into the relevant sections of the Hugo page that you want.

This is all the steps required for setting up ads on your Hugo website.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

## Conclusion

I hope this guide was useful for those of you who want to learn more about setting up ads on your website.

If your website isn't running on Hugo, you can just copy and paste the Adsense HTML code to your website directly instead of doing all the Hugo partial steps in this guide.