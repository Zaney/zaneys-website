---
title: "How to Create a Bootable USB in the Linux Terminal"
description: "Creating a bootable USB drive in Linux using the terminal is an efficient and reliable method. Whether you need to install an operating system, run a live distribution, or create recovery media, this guide will walk you through the process step-by-step."
summary: "Creating a bootable USB drive in Linux using the terminal is an efficient and reliable method. Whether you need to install an operating system, run a live distribution, or create recovery media, this guide will walk you through the process step-by-step."
categories: ["Software","Tools","How To"]
tags: ["Linux","Terminal"]
#externalUrl: ""
date: 2024-12-13
draft: false
authors:
  - Zaney
---

Creating a bootable USB drive in Linux using the terminal is an efficient and reliable method. Whether you need to install an operating system, run a live distribution, or create recovery media, this guide will walk you through the process step-by-step. By the end, you’ll have a clear understanding of how to prepare your USB drive using tools like dd and cp, along with tips to ensure success.

## What is a Bootable USB?

A bootable USB contains an operating system or software package that your computer can boot directly into, bypassing the installed OS. This is particularly useful for:

- Installing a new operating system.
- Testing Linux distributions.
- Running rescue tools.
- Updating BIOS or firmware.

## Preparing to Create a Bootable USB

Before you start, make sure you have the following:

### A USB Drive

- **Capacity:** Ensure your USB drive has sufficient storage space, typically at least 4 GB.
- **Backup Data:** Creating a bootable USB will erase all data on the drive. Back up any important files.

### ISO File

An ISO file is a disk image containing all the files required for booting. You can download ISO files from official websites of Linux distributions (e.g., Ubuntu, Fedora, or Debian).

### Administrative Privileges

You need sudo permissions to write directly to a USB device.

### Terminal Emulator

Open a terminal on your Linux system. This will be your workspace for creating the bootable USB.

## Identifying Your USB Drive

To avoid accidental data loss, it's crucial to correctly identify your USB drive.

### Step 1: Insert the USB Drive

Plug your USB drive into the computer and ensure it is detected.

### Step 2: List Storage Devices

Run the following command to list all connected storage devices:

```bash
lsblk
```

This will output a list of block devices, including their sizes and mount points. Look for your USB drive (e.g., `/dev/sdb` or `/dev/sdc`).

### Step 3: Confirm the Device

Double-check the device name using:

```bash
sudo fdisk -l
```

Ensure the size and description match your USB drive.

{{< alert >}}
**Warning:** Writing to the wrong device can result in permanent data loss on that drive.
{{< /alert >}}

## Using dd to Create a Bootable USB

The `dd` command is a powerful utility for copying and converting data. It’s commonly used to create bootable USB drives.

### Step 1: Unmount the USB Drive

Before writing to the USB drive, unmount it to prevent errors:

```bash
sudo umount /dev/sdX
```

Replace `/dev/sdX` with the correct device name for your USB drive.

### Step 2: Write the ISO File

Use the `dd` command to copy the ISO file to the USB drive:

```bash
sudo dd if=/path/to/your.iso of=/dev/sdX bs=4M status=progress
```

- `if=`: Input file, the path to your ISO file.
- `of=`: Output file, the USB device.
- `bs=4M`: Sets the block size to 4 MB for faster writing.
- `status=progress`: Displays progress during the operation.

### Step 3: Sync Data

To ensure all data is written to the USB drive, sync the filesystem:

```bash
sudo sync
```

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<!-- Display Ad -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="6031761775"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

## Verifying the Bootable USB

After creating the bootable USB, you can verify its contents:

### Mount the USB Drive

Remount the USB drive to check its files:

```bash
sudo mount /dev/sdX1 /mnt
```

Navigate to /mnt and verify the presence of bootable files.

### Test the USB Drive

Reboot your system and boot from the USB drive. Ensure your system is configured to boot from USB in the BIOS/UEFI settings.

## Using Alternative Tools

While `dd` is a powerful utility, there are other terminal-based tools for creating bootable USB drives:

### `cp` Command

For hybrid ISO files that support direct copying, you can use:

```bash
sudo cp /path/to/your.iso /dev/sdX
```

### `mkusb`

mkusb is a user-friendly tool for creating bootable USB drives. To install and use it:

```bash
sudo apt install mkusb
mkusb /path/to/your.iso
```

{{< alert >}}
**Warning:** The `sudo apt install` lines assume you are using Debian/Ubuntu. If you are using a different Linux distribution then this will need to be replaced by your distributions package managers install command. For example on Arch Linux the command would be `sudo pacman -S mkusb`. Please keep in mind different distributions of Linux also have slightly different package names.
{{< /alert >}}

Follow the on-screen prompts to complete the process.

### `woeusb` (for Windows ISOs)

To create bootable USB drives with Windows ISOs, use `woeusb`:

```bash
sudo apt install woeusb
woeusbgui
```

This provides a graphical interface for creating Windows bootable USB drives.

## Tips for Success

1. **Check ISO Integrity:** Verify the integrity of your ISO file using checksums (e.g., sha256sum).

```bash
sha256sum /path/to/your.iso
```

Compare the output with the checksum provided by the distribution’s website.

2. **Ensure Sufficient Permissions:** Always use sudo when accessing or writing to USB devices.

3. **Use Reliable USB Drives:** Low-quality USB drives may fail during the process or lead to boot issues.

4. **Backup Important Data:** Writing an ISO file will overwrite all existing data on the USB drive.

## Common Errors and Troubleshooting

### Error: Permission Denied

Ensure you’re using `sudo` for commands that require administrative privileges.

### Error: Device is Busy

This occurs if the USB drive is mounted. Unmount it using:

```bash
sudo umount /dev/sdX
```

### Boot Failure

- Double-check the ISO file’s integrity.
- Verify that your system’s BIOS/UEFI is set to boot from USB.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<!-- Display Ad -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="6031761775"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

## Conclusion

Creating a bootable USB drive in the Linux terminal is a straightforward process with powerful tools like `dd`. By following this guide, you can confidently prepare bootable USB drives for installing operating systems, running live environments, or performing system recovery. With practice and attention to detail, this skill will become an invaluable part of your Linux toolkit.

*I hope this helps you get more done in the linux terminal and helps you distro hop like a pro. 😁 Thank you for reading and have a wonderful day!*