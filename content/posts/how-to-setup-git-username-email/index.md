---
title: "How To Setup Git Username & Email"
description: "A quick and simple explaination for how to configure the global setting for git username and email."
summary: "A quick and simple explaination for how to configure the global setting for git username and email."
categories: ["How To","Tools"]
tags: ["Tutorial","Development","Git","Gitlab"]
#externalUrl: ""
date: 2024-12-11
draft: false
authors:
  - Zaney
---

Here's a quick explanation for installing Git on Windows and Linux:

## Windows Install

### 1. Download The Installer:

Go to <a target="_blank" href="https://git-scm.com/downloads/win">git-scm.com</a> and download the Git for Windows installer.

### 2. Run the Installer:

- Double-click the installer and follow the setup wizard.
- Choose default settings unless you have specific preferences (e.g., editor, PATH options).

### 3. Verify Installation:

- Open Command Prompt or PowerShell.
- Run:

```bash
git --version
```

## Linux Install

### 1. Open Terminal:

Every Linux distro comes with a terminal emulator. If you search for terminal in your applications you should find an option. Just open that and then move on to the next step.

### 2. Install Git:

- For Debian/Ubuntu:

```bash
sudo apt update && sudo apt install git
```

- For Fedora:

```bash
sudo dnf install git
```

- For Arch:

```bash
sudo pacman -S git
```

### 3. Verify Installation:

```bash
git --version
```

## Setting Git Username

Setting your Git user.name is actually quite simple. You just want to open a terminal and run this command:

```bash
git config --global user.name "My Name"
```

## Setting Git Email

This is just as simple as the step before, simply run:

```bash
git config --global user.email "myemail@example.com"
```