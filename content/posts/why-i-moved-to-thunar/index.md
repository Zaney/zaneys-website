---
title: "Why I Moved from PCManFM to Thunar"
description: "I want to dive into a change I recently made in my Linux setup that might surprise a few of you. I've switched from PCManFM to Thunar as my file manager.First off, PCManFM has been a solid companion on my Linux journey. It's lightweight, fast, and offers a great set of features. However, as I delved deeper into tweaking my workflow and optimizing my system, I found myself gravitating towards Thunar for a few key reasons."
summary: "I want to dive into a change I recently made in my Linux setup that might surprise a few of you. I've switched from PCManFM to Thunar as my file manager.First off, PCManFM has been a solid companion on my Linux journey. It's lightweight, fast, and offers a great set of features. However, as I delved deeper into tweaking my workflow and optimizing my system, I found myself gravitating towards Thunar for a few key reasons."
categories: ["Software","Tools",]
tags: ["Linux","Thunar","PCManFM"]
#externalUrl: ""
date: 2023-11-27
draft: false
authors:
  - Zaney
---

I want to dive into a change I recently made in my Linux setup that might surprise a few of you. I've _switched from PCManFM to Thunar_ as my file manager. Now, why the shift?

First off, <a target="_blank" href="https://en.wikipedia.org/wiki/PCMan_File_Manager">PCManFM</a> has been a solid companion on my Linux journey. It's lightweight, fast, and offers a great set of features. However, as I delved deeper into tweaking my workflow and optimizing my system, I found myself gravitating towards <a target="_blank" href="https://docs.xfce.org/xfce/thunar/start">Thunar</a> for a few key reasons.

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/l3q2UyW34cT2rcgko" width="100%" height="100%" frameBorder="0" style="position:absolute" class="giphy-embed" allowFullScreen></iframe></div>

- **Customization and Extensibility:** Thunar, despite being known for its simplicity, surprised me with its level of customization. The ability to add custom actions, plugins, and even scripts felt more accessible and robust compared to PCManFM. It opened up new possibilities to streamline my tasks and tailor my file manager to suit my workflow. _Also it looks incredible in comparison._ 😉
- **Integration and Stability:** While PCManFM worked well for basic file management, I noticed Thunar integrated more seamlessly with my system. Its stability and consistent performance, especially when handling larger files or dealing with various file types, made it a reliable choice for day-to-day usage.
- **Community Support and Development:** Thunar's active community and ongoing development were additional factors influencing my switch. Regular updates, bug fixes, and the community-driven nature of Thunar gave me confidence in its long-term support and continuous improvement. That's not to say PCManFM doesn't have a great community with active development though.
- **Feature Set and Flexibility:** While both PCManFM and Thunar offer a range of features, I found Thunar's balance between simplicity and functionality more aligned with my needs. Its tabbed browsing, dual-pane view, and advanced file management options provided the flexibility I sought without overwhelming me with unnecessary complexities.
- **Performance and Resource Usage:** Admittedly, PCManFM is known for its lightweight nature, but my experience with Thunar didn't significantly impact my system's performance. It ran smoothly, and the benefits it brought in terms of features and usability outweighed any marginal resource usage differences.

In essence, this switch from PCManFM to Thunar wasn't about one being inherently better than the other. Both file managers have their strengths and cater to different user preferences. For me, at this point in my Linux journey, Thunar aligns more closely with the customization, stability, and flexibility I want.

Remember, the beauty of the Linux ecosystem lies in the plethora of choices available to us. Experimenting, exploring, and finding what works best for your setup is what makes it all the more exciting.

What about you? Have you made a similar switch or found your favorite file manager? Share your thoughts in the comments below!

Until next time, happy tinkering and keep exploring the world of free and open source software!
