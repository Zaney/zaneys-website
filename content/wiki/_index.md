---
title: "ZaneyOS Documentation"
date: 2025-02-30
draft: false

cascade:
  showDate: false
  showAuthor: false
  invertPagination: true
---

{{< lead >}}
An incredible foundation for anyone looking to learn NixOS, with a beautiful modern aesthetic & easy to replicate system.
{{< /lead >}}
