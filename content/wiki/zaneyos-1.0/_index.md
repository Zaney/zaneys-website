---
title: "ZaneyOS 1.0 Documentation"
date: 2024-02-26
draft: false

cascade:
  showDate: false
  showAuthor: false
  invertPagination: true
---

{{< lead >}}
An incredible foundation for anyone looking to learn NixOS, with a beautiful modern aesthetic & easy to replicate system.
{{< /lead >}}

This section contains everything you need to know about ZaneyOS. If you're new, check out the [ZaneyOS Explained]({{< ref "wiki/zaneyos-1.0/my-nixos-config" >}}) page to begin.
