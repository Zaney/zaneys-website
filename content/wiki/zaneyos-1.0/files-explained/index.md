---
title: "How The Config Files Work"
description: "This article describes how the files inside the ZaneyOS project work. Which ones import each file, how the imports work, and many other questions are answered here."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 3
#externalUrl: ""
date: 2024-04-18
draft: false
authors:
  - Zaney
---

The way the files are structured is quite simple. The repository top level has the Gitlab specific files as well as the normal nix files that are responsible for modular imports. That means we have the normal configuration files, mine are just shortened names such as system.nix instead of configuration.nix.

Below you'll see the file structure:

```
├── config
│   ├── home
│   │   ├── bash.nix
│   │   ├── default.nix
│   │   ├── hyprland.nix
│   │   ├── files.nix
│   │   ├── files
│   │   │   ├── base16-themes
│   │   │   ├── emoji
│   │   │   └── starship.toml
│   │   └── ...
│   ├── system
│   │   ├── boot.nix
│   │   ├── default.nix
│   │   ├── displaymanager.nix
│   │   ├── packages.nix
│   │   └── ...
│   ├── scripts
│   │   ├── emopicker9000.nix
│   │   ├── wallsetter.nix
│   │   └── ...
│   └── pkgs
│       ├── sddm-sugar-dark.nix
│       └── ...
├── flake.nix
├── flake.lock
├── home.nix
├── hosts
│   └── default
│       ├── hardware.nix
│       └── options.nix
|── options.nix
├── system.nix
├── README.md
└── ...
```

## 🗄️ How Files Get Imported

I am using Home Manager, as a module, this is where a lot of the configuration for stuff is. As most things for theming are in user space you'll want to look for [Home Manager Options](https://nix-community.github.io/home-manager/options.xhtml) to see what you have available. To keep the home.nix from getting too large some of the configuration is split into separate files that home.nix imports. Those files are stored in the folder config/home and because the home.nix imports the folder as shown below there must be a default.nix file that then imports all the individual files in the folder desired.

![Home Manager Import config/home Folder](img/homemanagerimport.avif)

Now I am doing the same thing with the systems configuration too. You'll see a similar folder config/system that has files for configuring services, specific hardware driver stuff, and more. If you are hunting for the file that is the configuration.nix equivalent it is the system.nix file and the files in config/system.

## 🖋️ Why Have Config Files In Different Syntax!?

Having the configuration files as a .nix file allows me to use the Nix language to generate the actual desired config file. This allows me to use Nix Colors, **a phenomenal tool in NixOS**, to define a [base16 colorscheme](https://github.com/tinted-theming/base16-schemes) and let the user change the theme and have all the programs change upon rebuild. Even GTK applications change too!
