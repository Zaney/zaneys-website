---
title: "My NixOS Configuration: ZaneyOS Explained"
description: "I have chosen to use NixOS with the experimental Flakes feature. This allows me better version control over my system and the dependancy tree. I will explain a lot more about my Flake and how I use NixOS."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 1
#externalUrl: ""
date: 2024-04-18
draft: false
authors:
  - Zaney
---

## 👋 Preface

I have been using Linux for a long time and think I kept coming back to Arch because of its ease to troubleshoot and wide *(and up-to-date)* package availability. NixOS has all of that and more!

{{< typeit 
  tag=h3
  speed=50
  breakLines=false
>}}
Switching to NixOS has been truly an incredible time. It hasn't always been simple, but boy have I learned a lot!
{{< /typeit >}}

I am able to have the system build a lock file, using flakes, and this file tracks the specific version of everything installed for my system. So this means you can actually tell how long it has been since I have ran an update based on how old the last edit was to the flake.lock file on Gitlab! This feature means you won't encounter an issue with my flake because a new update broke a package on the unstable branch. Since when you installed you got the exact version of everything that I am running.

NixOS has a very different approach to how it operates compared to most other distributions. Essentially you use the Nix language to build out your systems configuration thats defined in specific .nix files, in the case of Flakes in a Git repository. This process is remarkably similar to building a binary. 

Because all of your programs are installed, services started, and options configured inside of Nix files this means NixOS is a declarative system. You declare the things you want to persist and be configured into the system instead of imperative meaning running a command to set persistent settings when needed. 

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/6YNgoTEPs6vZe" width="100%" height="100%" frameBorder="0" style="position:absolute" class="giphy-embed" allowFullScreen></iframe></div>

## 💎 The Goal

To have a easy to replicate system with all of the advanced tooling and theming that I think is awesome. 

I want people to have a NixOS system that shows a lot if not all that can be done with Nix, that's well organized, documented, and a great foundation to build off of.

{{< gitlab projectID="53038185" >}}

## 🤔 How Does It Work?

Many of the things you might want to do such as installing Distrobox or Flatpak, changing the terminal, even setting 24 hour clock can be done by changing a simple variable in the options.nix file.

![ZaneyOS Options File Example](img/optionsfile.avif)

When you run the rebuild switch command with the flake argument and give it the ZaneyOS folder, it sources the flake.nix that sources the system.nix and home.nix which all use the options.nix to set different configuration settings.

The hardware.nix is automatically generated with all the things you need and the system.nix imports it. The system.nix imports all the things inside config/system folder and home.nix does the same for the config/home folder. Some .nix files contain if statements that ensure the contents of the file are only used if certain variables are set like cpuType or gpuType.

## 🌟 Features

### ⌚ Polished Hyprland Experience

Enjoy the ability to have gorgeous animated borders around your windows, or if that may be to distracting for you simply disable it. [Hyprland](https://hyprland.org/) makes having incredible animations, transparency, blur, and so much more effortless.

![ZaneyOS Desktop Example](img/desktopdemo1.avif)

### 🖌️ One Color Scheme To Rule Them All

Using [NixColors](https://github.com/Misterio77/nix-colors) I am able to let you set the variable theme inside of the options.nix file that when you rebuild will change all of the programs on the system to match. This is because I use the [base16 colorscheme](https://github.com/tinted-theming/base16-schemes) you set there in the options file to set colors for everything from your terminal, to GTK apps.

### ⌨️ Easy To Understand Key-binds

{{< alert "bell" >}}
There is now a module in the waybar for displaying all the keybindings in ZaneyOS. If you change them, the changes are **not** reflected.
{{< /alert >}}

![Hyprland Keybindings Menu](img/keybindings.avif)

All of the key-binds are meant to make sense and be easy to remember. You can easily change them inside config/home/hyprland.nix if you want.

### 🖥️ Multi Host & User Configuration
- You can now define separate settings for different host machines and users!
- Easily specify extra packages for your users in the users/users.nix file.
- Easy to understand file structure where each host just has its generated hardware.nix file and option.nix file in a folder with the hostname as its title.

### 🏆 Handcrafted Text Editing Experience

I am so happy with my Neovim & Neovide configuration I am pretty confident that it can improve just about anyones efficiency in editing text.

![Neovim Never Looked So Good](img/neovimdemo1.avif)

I do plan on putting out a video and another post on some of the incredible things you can do with it. So please keep an eye out for that and I hope I am smart enough to come back and provide links for you! 🤣

#### 🔍 Searching Through Files Should Be Simple!

If you are working on a project you don't need to leave your text editor to load up a new file or search in your file browser. You can quickly press Space, then l, then g **in normal mode** to bring up a menu like so:

![Live Grep Demo](img/livegrepdemo.avif)

This menu lets you search through all of the files inside of wherever you launched Neovim / Neovide for whatever you type. This searches for text inside files, not the files name.

If you want to search for all files containing Neovim in the name you can press Space, then f, then f **in normal mode** to search for any file containing what you type.

### 🍫 Beautiful Bar With Many Styles

There are a growing number of themes for the Waybar to suit anyones theming preferences.

![Default Waybar Style](img/default.avif)
![Slickbar Numbered Style](img/slick-bar-num.avif)

## 📖 The Wiki

This is the first page of the new ZaneyOS documentation. You should be able to now see all the pages of the documentation and search for anything you need to. I hope this serves as a much better place to host the documentation for all of you!

## 🧐 Conclusion

I hope you will agree with Sheldon below.

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/xSJONQFKYodVtK5nwx" width="100%" height="100%" frameBorder="0" style="position:absolute" class="giphy-embed" allowFullScreen></iframe></div>

Please come over to my [Discord](https://discord.gg/2cRdBs8) and join the community helping each other and trying to make NixOS a truly great place to be.
