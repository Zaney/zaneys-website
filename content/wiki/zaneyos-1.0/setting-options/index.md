---
title: "Setting Options For Flake"
description: "Even though ZaneyOS is my configuration, I do provide more than just a few options to enable things I may not use myself."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 5
#externalUrl: ""
date: 2024-04-18
draft: false
authors:
  - Zaney
---

Even though ZaneyOS is my configuration, I do provide more than just a few options to enable things I may not use myself. You can request features and additions through the [repository's issues](https://gitlab.com/Zaney/zaneyos/-/issues). However, do keep in mind if I don't really want it or want to set it up I may choose not to implement it at all.

This page lists the options you may set and describes what they are. 

## 🔎 Where Are The Options??

They are located inside the options.nix file. Everything that you may want to change about my configuration is inside of the options.nix file.

### 🗞️ The Options.nix

| option | description |
| ------ | ------ |
| setUsername | Set The Users Name. |
| setHostname | Set The Systems Name On Network. |
| userHome | Where is your home directory? Typically located at /home/*insert-username-here*. |
| flakeDir | The location for the zaneyos / flakes directory. |
| wallpaperGit | The repository where you have all of your wallpapers. This will be used to populate the wallpaperDir directory. |
| wallpaperDir | Path to the directory to put and source wallpapers from inside of the users home directory. |
| screenshotDir | The folder where screenshots will be saved inside the users home directory. |
| flakePrev | Path to the previous zaneyos folder after updating with install script. |
| flakeBackup | Path to folder containing all zaneyos folder backups before the previous one. |
| gitUsername | Set username for Git, similar to *git config --global user.name "*insert-username-here*". |
| gitEmail | The email you use for your Git account. |
| theme | A [base16 colorscheme](https://github.com/tinted-theming/base16-schemes) to use with [NixColors](https://github.com/Misterio77/nix-colors) |
| borderAnim | Enables or disables the looped animated borders. This is distracting to some. |
| extraMonitorSettings | Add "monitor=" hyprland lines to hyprland config for host specific monitor settings. |
| waybarAnim | Determines whether or not the elements on waybar may be animated. |
| bar-number | Do you want the numbers in the waybar workspaces or not? |
| clock24h | Enable the time for different utilities to be presented in 24 hour format. |
| theLocale | Set the locale of the system. |
| theKBDLayout | Default keyboard layout for the system. |
| theSecondKBDLayout | Secondary switchable keyboard layout for the system. |
| theKBDVariant | Define a keyboard variant for the system. |
| theLCVariables | The extra locale options variable. Probably going to match your locale if in US. |
| theTimezone | Configure the timezone for the system. |
| theShell | Rather use zsh than bash? 😉 |
| theKernel | Choose which kernel you would like to run: default, latest, lqx, xanmod, and zen. |
| sdl-videodriver | Some games will need this to be set to x11, if this needs to be changed it should be set to wayland. |
| cpuType | The hardware.nix should take care of everything, but just in case there is something specific for Intel CPU's or AMD needed this exists. Also if running a virtual machine please set this to "*vm*" for the best experience. |
| gpuType | This enables or disables opengl and driver settings depending on whether or not it is amd, intel, or nvidia. There is also the option for intel-nvidia for hybrid laptop configurations. Just like cpuType, *if running a virtual machine please set this to "vm" to have a better experience. |
| intel-bus-id | If running hybrid intel-nvidia this is a requirement. Run, *sudo lshw -c display* and get the information needed. The syntax has to be changed slightly so [please refer here](https://nixos.wiki/wiki/Nvidia) for more information. |
| nvidia-bus-id | If running hybrid intel-nvidia this is a requirement. Run, *sudo lshw -c display* and get the information needed. The syntax has to be changed slightly so [please refer here](https://nixos.wiki/wiki/Nvidia) for more information. |
| nfs | Enable & start network filesystem. |
| nfsMountPoint | Where to mount the NFS at on the system. |
| nfsDevice | Specify the network device that you want to be mounted. |
| ntp | Enable time synchronization using pool.org. |
| localHWClock | Whether or not you want to use local hardware clock. This may break some applications. |
| printer | Enable printing and scanner support! |
| browser | The name of the browser do you want installed and used by default. The package name too, for ones like google-chrome where the binary is google-chrome-stable this is solved for you. So it would just be google-chrome. |
| terminal | Name of the binary for your terminal of choice. Example: "kitty" or "alacritty". |
| flatpak | Enables or disables the ability to use flatpaks. |
| kdenlive | Install kdenlive or not. Please keep in mind that this is not necessarily a small program. |
| blender | This option controls whether or not blender for 3D modeling is installed. |
| logitech | Enables added support for Logitech devices. Only needed for added configuration, your Logitech mouse or keyboard will continue to work with this disabled. |
| wezterm | Enable the installation of the wezterm Terminal Emulator. |
| alacritty | Enable the installation of the alacritty Terminal Emulator. |
| kitty | Enable the installation of the kitty Terminal Emulator. |
| python | Enable Python-dev and PyCharm installation. |
| syncthing | Install Syncthing. | 

### 🏙️ Wallpaper Git Repo

This is something that has been reported to confuse people. This repo is a link to my wallpaper repo that you can see on Gitlab.

You do **not** have to change this link. If you want my wallpapers then leave it there. Otherwise if you have your own wallpapers repo then you can replace mine with yours.

The wallpaper setting script does require and configure your wallpapers directory to be a Git repo so this link should only ever be replaced not deleted.
