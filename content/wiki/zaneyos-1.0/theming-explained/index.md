---
title: "How Theming Works"
description: "I can have the theme the user set in the options.nix used to generate a GTK theme as well! So when you change that one variable all you system programs will change upon a rebuild."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 4
#externalUrl: ""
date: 2024-04-18
draft: false
authors:
  - Zaney
---

Themes you have available for you to set in the options.nix are available here. Just take off the .yaml part of any of those names and use that as your theme variable. Do note that some programs like SwayNotificationCenter may require a relaunch to show changes.

## 🌆 Cool Stuffs

I can have the theme the user set in the options.nix used to generate a GTK theme as well! So when you change that one variable all you system programs will change upon a rebuild.

I do have a theme changing module up in the bar. It should have a paint roller icon. This will load up a rofi menu that you can use to select a theme. After you select a theme a popup will ask you for a password to rebuild the system, which it does after using sed to replace the old theme name with the one you selected in the options.nix file.

![Demo Image Of The Theme Selector](img/themedemo.avif)
