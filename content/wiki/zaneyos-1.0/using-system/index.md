---
title: "How To Use ZaneyOS"
description: "Most options that you are likely to want to change are defined in options.nix inside the repo. These are variables that are used to determine how things are configured later in other files."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 7
#externalUrl: ""
date: 2024-04-18
draft: false
authors:
  - Zaney
---

I have some very useful aliases that are available to you after switching to the flake.

- flake-update 

This runs a ```nix flake update``` on the flake directory. **You probably shouldn't run this.** This updates all packages and the lock file in the ZaneyOS flake. Only do this if you need to and know you need to, running this command opens you up to breaking changes. 

- flake-rebuild 

Runs a ```rebuild switch --flake``` on the flake directory. Used for updating the system after you have made changes to some of the files.

- gcCleanup

This removes all other generations other than the current. Great for cleaning up the system if you have been rebuilding and making changes a lot recently.

{{< alert >}}
For most system maintenance this is all that is required.
{{< /alert >}}

## 💽 Main Configuration

Most options that you are likely to want to change are defined in hosts/<your-hostname>/options.nix inside the ZaneyOS repository. These are variables that are used to determine how things are configured later in other files. This file is the only file you will need to edit if you're happy with what I have available for my system.

## 🦸 Getting Updates To ZaneyOS

I am not a Git wizard and so this script could probably be improved greatly. However, I am happy enough with this solution for now.

The install script that is ran to install ZaneyOS can also be used to to backup and get the newest updates. So just run it again and it will move the ZaneyOS folder/repo you have now to a backup folder in your .config folder and get the newest version from GitLab.

## 📦 Adding & Installing Extra Packages

You can find the array of packages being installed for your user specifically at config/home/packages.nix and for the system packages it's the config/system/packages.nix file. There is also the option to add the extra packages you want in the users/users.nix file. 

{{< alert >}}
You **MUST** use the proper NixOS package names.
{{< /alert >}}

Visit the [NixOS Packages](https://search.nixos.org/packages) page to search for the application you want. It is a good idea to check the NixOS Options at the top of that page as well for the package your searching for.

If the package you are installing needs extra configuration there is a decent chance there is a way of installing it, (enable = true;), through NixOS options or [Home Manager](https://nix-community.github.io/home-manager/options.xhtml).
