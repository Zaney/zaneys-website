---
title: "What Is NixOS"
description: "At the core of NixOS is a configuration file, written in the Nix language. This Nix expression is used to specify packages, users, groups, services, networking, and a whole set of other things typically configured on a Linux system."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["Documentation"]
series_order: 2
#externalUrl: ""
date: 2024-04-18
draft: false
authors:
  - Zaney
---

NixOS is a Linux distribution known for its unique approach to package management and system configuration. It uses the Nix package manager, which is based on a purely functional approach to managing software and system configurations.

In NixOS, the entire operating system configuration is described declaratively in a configuration file, allowing for reproducibility and easy rollbacks. This means that changes to the system can be tracked and reversed, making it robust and reliable for system administrators and developers.

From [Keir Williams](https://medium.com/version-1/explore-nixos-a-dive-into-the-world-of-declarative-linux-63735ee3be41): 
"At the core of NixOS is a configuration file, written in the Nix language. This Nix expression is used to specify packages, users, groups, services, networking, and a whole set of other things typically configured on a Linux system.

Due to the nature of the Nix language, the system is configured completely declaratively. You specify what you want to achieve on your system, instead of specifying the step-by-step commands required to accomplish it. Using NixOS’s built-in configuration options, you can specify how you want the system to look in a manner that is logical to you. How things are built is calculated by Nix itself."

Inside of NixOS those of you who are used to Linux distributions such as Arch where there is an extra repository with almost any package you may need, will be surprised. Nixpkgs has over 80,000 packages in its repositories with many of the packages you might otherwise need something like the AUR for.

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/8DBXcicbnEnMQ" width="100%" height="100%" frameBorder="0" style="position:absolute" class="giphy-embed" allowFullScreen></iframe></div>

## 🤔 Why Choose NixOS

Choosing NixOS often depends on specific needs and preferences. Here are some reasons why people opt for NixOS:

### 🗒️ Declarative Configuration

NixOS employs a declarative approach to system configuration. This means the entire system configuration is defined in a single file or files, making it reproducible and easier to manage. Changes are tracked and can be rolled back if needed.

### 📦 Functional Package Management

The Nix package manager ensures that each package and its dependencies are isolated and managed separately. This prevents conflicts between different versions of software and enables easy rollbacks to previous versions.

### 🏗️ Reproducibility

NixOS allows for consistent and reproducible environments, critical in development, testing, and deployment scenarios. It's particularly valuable in DevOps and CI/CD pipelines where consistent environments are essential.

### 👯 Multiple Environments and Rollbacks 

With NixOS, it's possible to have multiple versions of the same software installed simultaneously without conflicts. This flexibility is especially useful for developers or those requiring different versions of software for various projects.

### 🫂 Community and Customization

NixOS has an active community that contributes to its growth, sharing configurations, and offering support. Its customizable nature allows users to tailor their systems to their needs.

## 📂 What Is Home Manager?

Within the context of NixOS I did a good job of explaining that here on my YouTube channel:

{{< youtube bkGSoD1qVwU >}}
