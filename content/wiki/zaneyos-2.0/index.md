---
title: "ZaneyOS 2.0 Documentation"
description: "A fresh update to ZaneyOS, this includes many improvements and fixes. As well as simplifying a lot of the structure of the project."
tags: ["Docs","NixOS","ZaneyOS"]
#externalUrl: ""
date: 2024-04-19
draft: false
authors:
  - Zaney
---

## ⬇️ How To Install & Update

### 📜 Script:

This is the easiest and recommended way of starting out. The script is not meant to allow you to change every option that you can in the flake or help you install extra packages. It is simply here so you can get my configuration installed with as little chances of breakages and then fiddle to your hearts content!

The install script also functions as a method of updating ZaneyOS. The same command below can be used to backup the current flake to ~/.config/zaneyos-backups and install the current upstream one.

Simply copy this and run it:

```
nix-shell -p git curl
```

Then:

```
sh <(curl -L https://gitlab.com/Zaney/zaneyos/-/raw/stable-2.0/install-zaneyos.sh)
```

## 🤔 How Does It Work? 

When you run the rebuild switch command with the flake argument and give it the ZaneyOS folder, it sources the flake.nix that sources the config.nix in your specific hosts folder.

Which the config.nix file is going to pull in the users.nix and home.nix to set different configuration settings. The hardware.nix is automatically generated with all the things you need and the config.nix imports it.

All of your configuration files are in the hosts/<your-hostname> except for hyprland.nix and waybar.nix which are large configs that probably won't change between hosts. Those extra config files are in the config folder.

{{< typeit 
  tag=h3
  speed=50
  breakLines=false
>}}
🦾 Why Have Config Files In Different Syntax!?
{{< /typeit >}}


Having the configuration files as a .nix file allows me to use the Nix language to generate the actual desired config file. This allows me to use Nix Colors, **a phenomenal tool in NixOS**, to define a [base16 colorscheme](https://github.com/tinted-theming/base16-schemes) and let the user change the theme and have all the programs change upon rebuild. Even GTK applications change too!

## 📰 What Is NixOS & Why Choose It?

NixOS is a Linux distribution known for its unique approach to package management and system configuration. It uses the Nix package manager, which is based on a purely functional approach to managing software and system configurations.

In NixOS, the entire operating system configuration is described declaratively in a configuration file, allowing for reproducibility and easy rollbacks. This means that changes to the system can be tracked and reversed, making it robust and reliable for system administrators and developers.

From Keir Williams: “At the core of NixOS is a configuration file, written in the Nix language. This Nix expression is used to specify packages, users, groups, services, networking, and a whole set of other things typically configured on a Linux system.

Due to the nature of the Nix language, the system is configured completely declaratively. You specify what you want to achieve on your system, instead of specifying the step-by-step commands required to accomplish it. Using NixOS’s built-in configuration options, you can specify how you want the system to look in a manner that is logical to you. How things are built is calculated by Nix itself.”

Inside of NixOS those of you who are used to Linux distributions such as Arch where there is an extra repository with almost any package you may need, will be surprised. Nixpkgs has over 80,000 packages in its repositories with many of the packages you might otherwise need something like the AUR for.

Choosing NixOS often depends on specific needs and preferences. Here are some reasons why people opt for NixOS:

### 🗒️ Declarative Configuration

NixOS employs a declarative approach to system configuration. This means the entire system configuration is defined in a single file or files, making it reproducible and easier to manage. Changes are tracked and can be rolled back if needed.

### 📦 Functional Package Management

The Nix package manager ensures that each package and its dependencies are isolated and managed separately. This prevents conflicts between different versions of software and enables easy rollbacks to previous versions.

### 🏗️ Reproducibility

NixOS allows for consistent and reproducible environments, critical in development, testing, and deployment scenarios. It's particularly valuable in DevOps and CI/CD pipelines where consistent environments are essential.

### 👯 Multiple Environments and Rollbacks 

With NixOS, it's possible to have multiple versions of the same software installed simultaneously without conflicts. This flexibility is especially useful for developers or those requiring different versions of software for various projects.

### 🫂 Community and Customization

NixOS has an active community that contributes to its growth, sharing configurations, and offering support. Its customizable nature allows users to tailor their systems to their needs.

## 🏠 What Is Home Manager?

I think I did a good job of explaining that here on my YouTube channel:

{{< youtube bkGSoD1qVwU >}}

## 📔 How To Use ZaneyOS

I have some very useful aliases that are available to you after switching to the flake.

### 🐊 Update The Flakes Inputs & Packages

- flake-update 

This runs a ```nix flake update``` on the flake directory. **You probably shouldn't run this.** This updates all packages and the lock file in the ZaneyOS flake. Only do this if you need to and know you need to, running this command opens you up to breaking changes. 

- flake-rebuild 

Runs a ```rebuild switch --flake``` on the flake directory. Used for updating the system after you have made changes to some of the files.

- gcCleanup

This removes all other generations other than the current. Great for cleaning up the system if you have been rebuilding and making changes a lot recently.

{{< alert >}}
For most system maintenance this is all that is required.
{{< /alert >}}

### 📦 Adding & Installing Extra Packages

You can find the place to put packages to be installed for your user specifically at hosts/*insert-hostname*/users.nix and for the system packages it's the hosts/*insert-hostname*/config.nix file.

{{< alert >}}
You **MUST** use the proper NixOS package names.
{{< /alert >}}

Visit the [NixOS Packages](https://search.nixos.org/packages) page to search for the application you want. It is a good idea to check the NixOS Options at the top of that page as well for the package your searching for.

If the package you are installing needs extra configuration there is a decent chance there is a way of installing it, (enable = true;), through NixOS options or [Home Manager](https://nix-community.github.io/home-manager/options.xhtml).

### 🤯 Variables & Theming

In your host specific folder you will find the variables.nix file. This is where you can set variables that are used other places in the configuration. The link to finding the themes available is below.

| option | description |
| ------ | ------ |
| gitUsername | Set username for Git, similar to *git config --global user.name "*insert-username-here*". |
| gitEmail | The email you use for your Git account. |
| theme | A [base16 colorscheme](https://github.com/tinted-theming/base16-schemes) to use with [NixColors](https://github.com/Misterio77/nix-colors) |
| borderAnim | Enables or disables the looped animated borders. This is distracting to some. |
| extraMonitorSettings | Add "monitor=" hyprland lines to hyprland config for host specific monitor settings. |
| clock24h | Enable the time for different utilities to be presented in 24 hour format. |
| waybarAnimations | Determines whether or not the elements on waybar may be animated. |
| browser | The name of the browser do you want installed and used by default. The package name too, for ones like google-chrome where the binary is google-chrome-stable this is solved for you. So it would just be google-chrome. |
| terminal | Name of the binary for your terminal of choice. Example: "kitty" or "alacritty". |

## 📁 File Structure Explained 

The way the files are structured is quite simple. The repository top level has the Gitlab specific files as well as the flake.nix and flake.lock files that are responsible for managing the flake. Then we have the modules folder which contains modules that allow me to give you simple ```drivers.amd.enable = true;``` option that sets many other config settings, this helps keep the config.nix from looking like an absolute beast.

The hosts folder has separate hosts configurations with the folder containing the files named by the hostname of that system. These could be put in home.nix and are being imported by it. However, these being in that file would make it significantly larger.

Below you'll see the file structure:

```
├── config
│   ├── emoji
│   ├── hyprland.nix
│   ├── waybar.nix
│   ├── base16-themes
│   ├── starship.toml
│   └── ...
├── modules
│   ├── amd-drivers.nix
│   ├── intel-drivers.nix
│   ├── local-hardware-clock.nix
│   ├── vm-guest-services.nix
│   └── ...
├── scripts
│   ├── emopicker9000.nix
│   ├── wallsetter.nix
│   └── ...
├── pkgs
│   ├── sddm-sugar-dark.nix
│   └── ...
├── flake.nix
├── flake.lock
├── hosts
│   └── default
│       ├── config.nix
│       ├── hardware.nix
│       ├── home.nix
│       ├── users.nix
│       └── variables.nix
├── README.md
└── ...
```

