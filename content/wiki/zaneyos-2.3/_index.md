---
title: "ZaneyOS 2.3 Documentation"
date: 2025-01-30
draft: false

cascade:
  showDate: false
  showAuthor: false
  invertPagination: true
---

{{< lead >}}
An incredible foundation for anyone looking to learn NixOS, with a beautiful modern aesthetic & easy to replicate system.
{{< /lead >}}

This section contains everything you need to know about ZaneyOS. If you're new, check out the [ZaneyOS Explained]({{< ref "wiki/zaneyos-2.3/my-nixos-config" >}}) page to begin.
