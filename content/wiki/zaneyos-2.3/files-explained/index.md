---
title: "How The Config Files Work"
description: "This article describes how the files inside the ZaneyOS project work. Which ones import each file, how the imports work, and many other questions are answered here."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["ZaneyOS 2.3 Docs"]
series_order: 2
#externalUrl: ""
date: 2025-01-30
draft: false
authors:
  - Zaney
---

The way the files are structured is quite simple. The repository top level has the Gitlab specific files, the `hosts` folder for separate host configurations, the `config` folder for a lot of extra program configs that shouldn't change between hosts, and the `flake.nix` file. That means we have the normal configuration files, mine are just shortened names such as `system.nix` instead of `configuration.nix` down in the `hosts/<your-hostname>` folder.

Then we have the `modules` folder which contains modules that allow me to give you simple `drivers.amd.enable = true;` option that sets many other config settings, this helps keep the `config.nix` from looking even more like an absolute beast.

Below you'll see the file structure:

```
├── config
│   ├── emoji
│   ├── hyprland.nix
│   ├── waybar.nix
│   ├── starship.toml
│   └── ...
├── modules
│   ├── amd-drivers.nix
│   ├── intel-drivers.nix
│   ├── local-hardware-clock.nix
│   ├── vm-guest-services.nix
│   └── ...
├── scripts
│   ├── emopicker9000.nix
│   ├── wallsetter.nix
│   └── ...
├── flake.nix
├── flake.lock
├── hosts
│   └── default
│       ├── config.nix
│       ├── hardware.nix
│       ├── home.nix
│       ├── users.nix
│       └── variables.nix
├── README.md
└── ...
```

## 🗄️ How Files Get Imported

I am using Home Manager, as a module, this is where a lot of the configuration for stuff is. As most things for theming are in user space you'll want to look for [Home Manager Options](https://nix-community.github.io/home-manager/options.xhtml) to see what you have available. To keep the `home.nix` from getting too large some of the configuration is split into separate files that `home.nix` imports.

## 🖋️ Why Have Config Files In Different Syntax!?

Having the configuration files as a `.nix` file allows me to use the [Nix](https://nix.dev/tutorials/nix-language.html) language to generate the actual desired config file. This allows me to use Stylix, **a phenomenal tool in NixOS**, to define a colorscheme and let the user change the theme and have all the programs change upon rebuild. Even GTK applications change too!
