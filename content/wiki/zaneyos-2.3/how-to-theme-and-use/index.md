---
title: "How To Theme & Use ZaneyOS"
description: "Using Stylix I can have the theme generated off an image supplied in the system.nix file! So when you change that one variable all you system programs will change upon a rebuild."
tags: ["Docs","NixOS","ZaneyOS"]
series: ["ZaneyOS 2.3 Docs"]
series_order: 3
#externalUrl: ""
date: 2025-01-30
draft: false
authors:
  - Zaney
---

Inside the `hosts/<your-hostname>/system.nix` file you will see a section beginning with *stylix*. This section includes a line that references an image in the flake. [Stylix](https://github.com/danth/stylix) needs an image no matter what and it will use it to generate a color scheme for the system.

If you would like to define your color scheme you can uncomment the lines below the image line and those settings will override the image setting.

## 📔 How To Use ZaneyOS

I have some very useful aliases that are available to you after switching to the flake.

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3548718965338637"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3548718965338637"
     data-ad-slot="9695674610"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

### 🐊 Update The Flakes Inputs & Packages

- fu

This runs a ```nix flake update``` on the flake directory. **You probably shouldn't run this.** This updates all packages and the lock file in the ZaneyOS flake. Only do this if you need to and know you need to, running this command opens you up to breaking changes. 

- fr

Runs a ```rebuild switch --flake``` on the flake directory. Used for updating the system after you have made changes to some of the files. This assumes you keep the zaneyos folder/repo in your home dir.

- ncg

This removes all other generations other than the current. Great for cleaning up the system if you have been rebuilding and making changes a lot recently.

- zu

This will pull down the latest stable install script from the zaneyos repo and run it. The install script also functions as an update script where it backups your current config to .config/zaneyos-backups and then installs the new version.

{{< alert >}}
For most system maintenance this is all that is required.
{{< /alert >}}

The ```cat``` command has been aliased to ```bat``` which makes life awesome when you want to quickly check a file. I have also added a few interesting terminal easter eggs that are pretty neat.

### 📦 Adding & Installing Extra Packages

You can find the place to put packages to be installed for your user specifically at `hosts/<your-hostname>/users.nix` and for the system packages it's the `hosts/<your-hostname>/config.nix` file.

{{< alert >}}
You **MUST** use the proper NixOS package names.
{{< /alert >}}

Visit the [NixOS Packages](https://search.nixos.org/packages) page to search for the application you want. It is a good idea to check the NixOS Options at the top of that page as well for the package your searching for.

If the package you are installing needs extra configuration there is a decent chance there is a way of installing it, (enable = true;), through NixOS options or [Home Manager](https://nix-community.github.io/home-manager/options.xhtml).

### 🤯 Variables

In your host specific folder you will find the `variables.nix` file. This is where you can set variables that are used other places in the configuration.

| option | description |
| ------ | ------ |
| gitUsername | Set username for Git, similar to `git config --global user.name "<your-username>"`. |
| gitEmail | The email you use for your Git account. |
| extraMonitorSettings | Add "monitor=" hyprland lines to hyprland config for host specific monitor settings. |
| clock24h | Enable the time for different utilities to be presented in 24 hour format. |
| browser | The name of the browser do you want installed and used by default. The package name too, for ones like google-chrome where the binary is google-chrome-stable this is solved for you. So it would just be google-chrome. |
| terminal | Name of the binary for your terminal of choice. Example: "kitty" or "alacritty". |
| keyboardLayout | Set the keyboard layout, defaults to "us" |
| consoleKeyMap | Set the console layout, defaults to "us" |

